package com.jeclr.android.utils;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.Map;

public class SimpleHttpRequestor extends HttpRequestor {

    public SimpleHttpRequestor(String baseUrl) {
        super(baseUrl);
    }

    @Override
    protected HttpURLConnection getConnection(String url) throws IOException {
        HttpURLConnection conn = super.getConnection(url);
        conn.setConnectTimeout(TIMEOUT);
        conn.setDoOutput(true);
        conn.setUseCaches(false);
        conn.setInstanceFollowRedirects(false);
        conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        return conn;
    }

    @Override
    protected HttpURLConnection post(String url, String body, Map<String, String> header)
            throws IOException {

        byte[] bytes = body.getBytes();
        HttpURLConnection conn = getConnection(url);
        conn.setFixedLengthStreamingMode(bytes.length);
        conn.setRequestMethod("POST");

        if (header != null) {
            for (String key : header.keySet()) {
                conn.setRequestProperty(key, header.get(key));
            }
        }
        try (OutputStream out = conn.getOutputStream()) {
            out.write(bytes);
        }
        return conn;
    }

    @Override
    protected HttpURLConnection get(String url, String body, Map<String, String> header) throws IOException {
        HttpURLConnection conn = getConnection(url);
        conn.setRequestMethod("GET");

        if (header != null) {
            for (String key : header.keySet()) {
                conn.setRequestProperty(key, header.get(key));
            }
        }
        return conn;
    }

    @Override
    protected HttpURLConnection delete(String url, String body, Map<String, String> header) throws IOException {
        HttpURLConnection conn = getConnection(url);
        conn.setRequestMethod("DELETE");

        if (header != null) {
            for (String key : header.keySet()) {
                conn.setRequestProperty(key, header.get(key));
            }
        }

        return conn;
    }

    @Override
    protected HttpURLConnection put(String url, String body, Map<String, String> header) throws IOException {
        byte[] bytes = body.getBytes();
        HttpURLConnection conn = getConnection(url);
        conn.setRequestMethod("PUT");

        if (header != null) {
            for (String key : header.keySet()) {
                conn.setRequestProperty(key, header.get(key));
            }
        }

        try (OutputStream out = conn.getOutputStream()) {
            out.write(bytes);
        }

        return conn;
    }

    @Override
    protected HttpURLConnection patch(String url, String body, Map<String, String> header) throws IOException {
        byte[] bytes = body.getBytes();
        HttpURLConnection conn = getConnection(url);
        conn.setFixedLengthStreamingMode(bytes.length);
        conn.setRequestMethod("PATCH");

        if (header != null) {
            for (String key : header.keySet()) {
                conn.setRequestProperty(key, header.get(key));
            }
        }

        try (OutputStream out = conn.getOutputStream()) {
            out.write(bytes);
        }

        return conn;
    }
}
