package com.jeclr.android.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class HttpRequestor {

    protected String baseUrl;
    private static final Logger LOGGER = Logger.getLogger(HttpRequestor.class.getName());
    /**
     * Initial delay before first retry, without jitter.
     */
    protected static final int BACKOFF_INITIAL_DELAY = 1000;
    /**
     * Maximum delay before a retry.
     */
    protected static final int MAX_BACKOFF_DELAY = 1024000;

    protected final Random random = new Random();
    protected static final int TIMEOUT = 60000;

    public HttpRequestor(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    protected abstract HttpURLConnection post(String url, String body, Map<String, String> header)
            throws IOException;

    protected abstract HttpURLConnection get(String url, String body, Map<String, String> header)
            throws IOException;

    protected abstract HttpURLConnection delete(String url, String body, Map<String, String> header)
            throws IOException;

    protected abstract HttpURLConnection put(String url, String body, Map<String, String> header)
            throws IOException;

    protected abstract HttpURLConnection patch(String url, String body, Map<String, String> header)
            throws IOException;

    public Result send(String url, String httpMethod, String body, Map<String, String> header, int retries)
            throws IOException {
        int attempt = 0;
        Result result = null;
        int backoff = BACKOFF_INITIAL_DELAY;
        boolean tryAgain;
        do {
            attempt++;
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "Attempt #{0} to send data\n {1}", new Object[]{attempt, body});
            }
            result = sendNoRetry(url, httpMethod, body, header);
            tryAgain = result == null && attempt <= retries;
            if (tryAgain) {
                int sleepTime = backoff / 2 + random.nextInt(backoff);
                sleep(sleepTime);
                if (2 * backoff < MAX_BACKOFF_DELAY) {
                    backoff *= 2;
                }
            }
        } while (tryAgain);
        if (result == null) {
            throw new IOException("Could not send message after " + attempt + " attempts");
        }
        return result;
    }

    public Result sendNoRetry(String url, String httpMethod, String body, Map<String, String> header) throws IOException {

        if (url == null) {
            LOGGER.log(Level.WARNING, "Sending request to base url: {0}", url);
        } else {
            url = baseUrl + url;
        }
        if (!baseUrl.startsWith("https://")) {
            LOGGER.log(Level.WARNING, "URL does not use https: {0}", url);
        }

        HttpURLConnection conn = makeRequest(url, httpMethod, body, header);
        if (conn == null) {
            return null;
        }
        final int responseCode = conn.getResponseCode();
        if (responseCode >= 500) {
            return null;
        }

        String responseBody = getString(conn.getInputStream());

        LOGGER.log(Level.FINEST, "JSON error response: {0}", responseBody);
        return new Result(conn, responseBody);
    }

    private HttpURLConnection makeRequest(String url, String httpMethod, String body, Map<String, String> header) throws IOException {
        HttpURLConnection conn = null;
        switch (httpMethod) {
            case "POST":
                conn = post(url, body, header);
                break;
            case "PUT":
                conn = put(url, body, header);
                break;
            case "DELETE":
                conn = delete(url, body, header);
                break;
            case "PATCH":
                conn = patch(url, body, header);
                break;
            case "GET":
                conn = get(url, body, header);
                break;
        }
        return conn;
    }

    protected static String getString(InputStream stream) throws IOException {
        BufferedReader reader
                = new BufferedReader(new InputStreamReader(nonNull(stream)));
        StringBuilder content = new StringBuilder();
        String newLine;
        do {
            newLine = reader.readLine();
            if (newLine != null) {
                content.append(newLine).append('\n');
            }
        } while (newLine != null);
        if (content.length() > 0) {
            // strip last newline
            content.setLength(content.length() - 1);
        }
        return content.toString();
    }

    static <T> T nonNull(T argument) {
        if (argument == null) {
            throw new IllegalArgumentException("argument cannot be null");
        }
        return argument;
    }

    protected HttpURLConnection getConnection(String url) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
        return conn;
    }

    void sleep(long millis) {
        try {
            Thread.sleep(millis);
        }
        catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public String createQuery(Map<String, Object> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (String item : params.keySet()) {
            if (first) {
                first = false;
            } else {
                result.append("&");
            }
            result.append(URLEncoder.encode(item, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(params.get(item).toString(), "UTF-8"));
        }
        return result.toString();
    }

    public class Result {

        private HttpURLConnection connection;
        private String responseBody;

        public Result(HttpURLConnection connection, String responseBody) {
            this.connection = connection;
            this.responseBody = responseBody;
        }

        public HttpURLConnection getConnection() {
            return connection;
        }

        public void setConnection(HttpURLConnection connection) {
            this.connection = connection;
        }

        public String getResponseBody() {
            return responseBody;
        }

        public void setResponseBody(String responseBody) {
            this.responseBody = responseBody;
        }

    }

}
